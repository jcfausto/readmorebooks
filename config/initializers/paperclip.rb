#http://stackoverflow.com/questions/9646549/default-url-in-paperclip-broke-with-asset-pipeline-upgrade

Paperclip.interpolates(:placeholder) do |attachment, style|
  ActionController::Base.helpers.asset_path("book_#{style}.png")
end

#using AWS only in production
if Rails.env.production?
  #https://devcenter.heroku.com/articles/paperclip-s3
  Paperclip::Attachment.default_options[:url] = ':s3_domain_url'
  Paperclip::Attachment.default_options[:path] = '/:class/:attachment/:id_partition/:style/:filename'
  #Paperclip::Attachment.default_options[:s3_host_name] = 'readmorebooksapp.s3.amazonaws.com'
end
