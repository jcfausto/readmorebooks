Rails.application.routes.draw do
  resources :purchases
  resources :books

  devise_for :users, controllers: { confirmations: 'confirmations' }

  get 'users/:id' => 'users#show', as: :user

  get 'purchases' => 'users#purchases', as: :user_purchases

  get 'book/:id/buy' => 'books#buy', as: :book_purchase

  root 'books#index'

end
