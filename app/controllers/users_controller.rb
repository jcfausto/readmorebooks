class UsersController < ApplicationController

  def show
    @user = User.find(params[:id])
    @books = @user.books.page(params[:page]).per_page(12)
  end

  def purchases
    @user = User.find(current_user.id)
    @purchases = @user.purchases
  end

end
