#After confirmation, the user will be redirected to profile, in order to fill it.
class ConfirmationsController < Devise::ConfirmationsController

  private

  def after_confirmation_path_for(resource_name, resource)
    flash[:notice] = 'Conta confirmada com sucesso. Complete seu perfil antes de prosseguir e aproveite!'
    edit_user_registration_path
  end

end
