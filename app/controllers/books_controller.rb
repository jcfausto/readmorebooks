class BooksController < ApplicationController
  before_action :set_book, only: [:edit, :update, :destroy]


  def buy
    @book = Book.find(params[:id])

    if @book
      @purchase = current_user.purchases.new
      @purchase.book_id = @book.id
      @purchase.save

      @book.in_transaction = true
      @book.purchase_id = @purchase.id

      respond_to do |format|
        if @book.save
          format.html { redirect_to @book, notice: 'Livro reservado com sucesso. Verifique seu e-mail para continuar a compra.' }
          format.json { render :show, status: :created, location: @book }
        else
          @purchase.destroy
          format.html { render :new }
          format.json { render json: @book.errors, status: :unprocessable_entity }
        end
      end
    else
       respond_to do |format|
        format.html { redirect_to @book, notice: 'Livro não encontrado!' }
       end
    end
  end

  # GET /books
  # GET /books.json
  def index
    @books = Book.order("created_at desc").page(params[:page]).per_page(12)
  end

  # GET /books/1
  # GET /books/1.json
  def show
    @book = Book.find(params[:id])
  end

  # GET /books/new
  def new
    @book = current_user.books.new
  end

  # GET /books/1/edit
  def edit
  end

  # POST /books
  # POST /books.json
  def create
    @book = current_user.books.new(book_params)

    respond_to do |format|
      if @book.save
        format.html { redirect_to @book, notice: 'Book was successfully created.' }
        format.json { render :show, status: :created, location: @book }
      else
        format.html { render :new }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /books/1
  # PATCH/PUT /books/1.json
  def update
    respond_to do |format|
      if @book.update(book_params)
        format.html { redirect_to @book, notice: 'Book was successfully updated.' }
        format.json { render :show, status: :ok, location: @book }
      else
        format.html { render :edit }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /books/1
  # DELETE /books/1.json
  def destroy
    @book.destroy
    respond_to do |format|
      format.html { redirect_to books_url, notice: 'Book was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_book
      @book = current_user.books.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def book_params
      params.require(:book).permit(:title, :author, :year, :pages, :notes, :user_id, :image, :for_sale, :in_transaction, :sold)
    end
end
