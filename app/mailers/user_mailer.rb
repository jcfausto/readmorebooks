class UserMailer < ApplicationMailer
  default from: 'lermaislivrosapp@gmail.com'

  def welcome_email(user)
    @user = user
    @url  = 'http://lermaislivros.com/'
    mail(to: @user.email, subject: 'Seja Muito Bem-Vindo(a) Ao Ler Mais Livros!')
  end
end
