class ApplicationMailer < ActionMailer::Base
  default from: "lermaislivrosapp@gmail.com"
  layout 'mailer'
end
