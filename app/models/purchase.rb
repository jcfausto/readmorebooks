class Purchase < ActiveRecord::Base
  has_one :book, dependent: :nullify
  belongs_to :user
end
