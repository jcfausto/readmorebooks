class Profile < ActiveRecord::Base
  belongs_to :user

  def full_name
   [self.first_name, self.last_name].reject(&:blank?).join(" ")
  end

  def to_s
    full_name
  end
end
