class User < ActiveRecord::Base

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable

  #relationship with user
  has_many :books
  has_one  :profile, dependent: :destroy
  has_many :purchases

  accepts_nested_attributes_for :profile

  def to_s
    self.profile.full_name
  end

  #http://www.rubydoc.info/github/plataformatec/devise/Devise/Models/Confirmable
  def after_confirmation
    welcome_email
  end

  private

  def welcome_email
    UserMailer.welcome_email(self).deliver_now
  end

end
