class Book < ActiveRecord::Base
  belongs_to :user
  belongs_to :purchase

  #must have a description
  validates :title, presence: true, length: { maximum: 120 }
  validates :user_id, presence: true

  #If more styles were added, don't forget to include a default image for that style
  #at app/assets/images/missing_STYLE.png. See also de config/initializers/paperclip.rb
  has_attached_file :image, :default_url => "book_:style.png", styles: {thumb: "160x213", medium: "250x333"}

  validates_attachment :image, presence: true,
                            content_type: { content_type: ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'] },
                            size: { less_than: 2.megabytes }

  def to_s
    self.title
  end

end
