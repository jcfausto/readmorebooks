json.array!(@purchases) do |purchase|
  json.extract! purchase, :id, :book_id, :buyer_id
  json.url purchase_url(purchase, format: :json)
end
