json.array!(@books) do |book|
  json.extract! book, :id, :title, :author, :year, :pages, :notes, :user_id
  json.url book_url(book, format: :json)
end
