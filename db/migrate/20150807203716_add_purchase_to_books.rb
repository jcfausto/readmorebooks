class AddPurchaseToBooks < ActiveRecord::Migration
  def change
    add_reference :books, :purchase, index: true, foreign_key: true
  end
end
