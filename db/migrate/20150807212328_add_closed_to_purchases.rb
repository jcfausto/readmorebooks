class AddClosedToPurchases < ActiveRecord::Migration
  def change
    add_column :purchases, :closed, :boolean, :default => false
  end
end
