class AddInTransactionToBooks < ActiveRecord::Migration
  def change
    add_column :books, :in_transaction, :boolean, :default => false
  end
end
